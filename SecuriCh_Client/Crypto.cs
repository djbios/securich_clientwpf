﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SecuriCh_Client
{
   static class Crypto //Класс, реализующий криптографию
    {
        public static string CryptString(string input)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            UnicodeEncoding byteConverter = new UnicodeEncoding();
            
            byte[] orig = byteConverter.GetBytes(input);
            byte[] encrypted = rsa.Encrypt(orig, false);
            byte[] decrypted = rsa.Decrypt(encrypted, false);
            string output = byteConverter.GetString(decrypted);
            return output;
        }

        static public void GenerateRSA(ref string pubicKey,ref string privateKey)
        {
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                privateKey = rsa.ToXmlString(true);
                pubicKey = rsa.ToXmlString(false);

            }
        }

        static public byte[] RsaEncrypt(byte[] dataToEncrypt, RSAParameters rsaKeyInfo, bool doOaepPadding)
        {
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
                {

                    //Import the RSA Key information. This only needs
                    //toinclude the public key information.
                    rsa.ImportParameters(rsaKeyInfo);

                    //Encrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    encryptedData = rsa.Encrypt(dataToEncrypt, doOaepPadding);
                }
                return encryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }

        }

        static public byte[] RsaDecrypt(byte[] dataToDecrypt, string xmlstringprivate, bool doOaepPadding)
        {
            try
            {
                byte[] decryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This needs
                    //to include the private key information.
                  //  RSA.ImportParameters(RSAKeyInfo);
                    rsa.FromXmlString(xmlstringprivate);
                    //Decrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    decryptedData = rsa.Decrypt(dataToDecrypt, doOaepPadding);
                }
                return decryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());

                return null;
            }

        }

        static public string RsaEncryptString(string dataToEncrypt, string xmlstringOpenkey, bool doOaepPadding)
        {
            UnicodeEncoding byteConverter = new UnicodeEncoding();
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This only needs
                    //toinclude the public key information.
                  //  RSA.ImportParameters(RSAKeyInfo);
                    rsa.FromXmlString(xmlstringOpenkey);
                    //Encrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    encryptedData = rsa.Encrypt(byteConverter.GetBytes(dataToEncrypt), doOaepPadding);
                }
                return byteConverter.GetString(encryptedData);
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }

        }

        static public string RsaDecryptString(string dataToDecrypt, string xmlprivate, bool doOaepPadding)
        {
             UnicodeEncoding byteConverter = new UnicodeEncoding();
            try
            {
                byte[] decryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This needs
                    //to include the private key information.
                  //  RSA.ImportParameters(RSAKeyInfo);
                    rsa.FromXmlString(xmlprivate);
                    //Decrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    decryptedData = rsa.Decrypt(byteConverter.GetBytes(dataToDecrypt), doOaepPadding);
                }
                return byteConverter.GetString(decryptedData);
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());

                return null;
            }

        }
    }
}
