﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecuriCh_Client
{
    class Message
    {
        public string text {get;set;}
        public User author {get;set;}
        public bool my { get; set; }

         public Message(User author1, string text1) //Сообщение собеседнка
        {
            text = text1;
            author= author1;
            my = false;
        }

        public Message(string text1) //мое сообщение
        {
            text = text1;
            author = new User() ;
            my = true;
        }

        public string GetCrypted()
        {
            return Crypto.RsaEncryptString(this.text, author.pubkey, false);
        }
    }
}
