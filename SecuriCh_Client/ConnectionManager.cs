﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
// ReSharper disable InconsistentNaming

namespace SecuriCh_Client
{
    class ConnectionManager
    {
        private Mutex mtx = new Mutex();
        private bool stop;
        volatile static TcpClient tcpclient = new TcpClient();
        private string nick;
        private string xmlprivate;
        private string xmlpublic;
        private uint ID;
        private  List<User> users = new List<User>();
        readonly UnicodeEncoding byteConverter = new UnicodeEncoding();

       public ConnectionManager()
        {
           Initialize();
           stop = false;
           Thread clientThread = new Thread(ClientThread);
           clientThread.Start();
        }

    

        private void SendZapros(byte type, int lengthOfMessage, byte[] приложение) //отправляет запрос серверу
        {
            mtx.WaitOne();
            if (!tcpclient.Connected)
            {
                TryToConnect();
            }
            NetworkStream stream = tcpclient.GetStream();
            byte[] zap = new byte[6];
            zap[0] = 13;
            zap[1] = type;
            byte[] intlen = BitConverter.GetBytes(lengthOfMessage);
            zap[2] = intlen[0];
            zap[3] = intlen[1];
            zap[4] = intlen[2];
            zap[5] = intlen[3];
            stream.Write(zap, 0, 6);
            if (приложение != null)
                stream.Write(приложение, 0, приложение.Length);
            mtx.ReleaseMutex();
        }

        
        byte[] GetPacket(int length)
        {
            mtx.WaitOne();
            if (tcpclient.Connected)
            {
               
                var stream = tcpclient.GetStream();
                byte[] someBytes = new byte[length];
                bool received=false;
                while (!received)
                {
                    if (stream.DataAvailable)
                        try
                        {
                            stream.ReadTimeout = 6000;
                            stream.Read(someBytes, 0, 1);
                            received = true;
                            return someBytes;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            CheckConnection();
                        }
                }
            }
            else
            {
                TryToConnect();
            }
            mtx.ReleaseMutex();
            return null;
        }

        private void CheckConnection()
        {
            if (!tcpclient.Connected)
                TryToConnect();
        }

        private void Initialize() //Процесс инициализации при подключении к серверу
        {
            if ((xmlpublic == null) || (xmlprivate == null))
                Crypto.GenerateRSA(ref xmlpublic, ref xmlprivate); //генерим ключи, если не сгенерены
            if (nick == null)
                nick = "Parnishka"; //Здесь сделать забор ника из формы
            byte[] pubKey = byteConverter.GetBytes(xmlpublic);
            byte[] nickbBytes = byteConverter.GetBytes(nick);
            SendZapros(1, pubKey.Length, pubKey); //отправка публичного ключа
            SendZapros(1, nickbBytes.Length, nickbBytes); //отправка ника
            byte[] idbytes = GetPacket(sizeof (int));
            if(idbytes!=null)
                ID = BitConverter.ToUInt32(idbytes, 0); //получение ID
            else
            {
                MessageBox.Show("Error receiving ID");
            }
        }



        void ClientThread()
        {
            
            while ((!stop))
            {
                if (tcpclient.Connected)
                {
                    //тело рабочего процесса 
                    
                }
                else
                {
                    //попытка переподключения при обрыве
                    TryToConnect();
                }
               
            }
            if (!tcpclient.Connected)
            {
                MessageBox.Show("Не удается подключиться к серверу " + Transit.ServerIP + ":" + Transit.ServerPort.ToString());

            }
        }

        void GetUsersList()
        {
            users.Clear();
            SendZapros(2, 0, null);
            int count = BitConverter.ToInt32(GetPacket(sizeof (int)), 0);
            for (int i = 0; i < count; i++)
            {
                uint nowID = BitConverter.ToUInt32(GetPacket(sizeof(uint)),0);
                int sizeofnick = BitConverter.ToInt32(GetPacket(sizeof(int)), 0);
                string nownick = byteConverter.GetString(GetPacket(sizeofnick), 0, sizeofnick);
                int sizeofPK = BitConverter.ToInt32(GetPacket(sizeof(int)), 0);
                string nowPK = byteConverter.GetString(GetPacket(sizeofPK), 0, sizeofPK);
                User nowUser = new User()
                {
                    id = nowID,
                    nick = nownick,
                    pubkey = nowPK
                };
                users.Add(nowUser);
            }

        }

        void SendMessage(Message msg, User user)
        {

        }

        void TryToConnect()
        {
            for (var i = 0; i < Transit.connection_tryings; i++)
                if (!tcpclient.Connected)
                    try
                    {
                        tcpclient.Connect(Transit.ServerIP, Transit.ServerPort);
                        break;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    
        }

       
    }
}
