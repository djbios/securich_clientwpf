﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SecuriCh_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
       List<Button> msgbtlst = new List<Button>();
       // private ConnectionManager CM = new ConnectionManager();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //ConnectionManager CM = new ConnectionManager();
            List<Message> messages = new List<Message>();
            listBox2.Items.Clear();
            User vasya = new User();
            vasya.nick = "vasya";
            vasya.pubkey = "asd";
            string pk="";
            string prk="";
            Crypto.GenerateRSA(ref pk, ref prk);
            vasya.pubkey = pk;
            vasya.id = 14;

            messages.Add(new Message(vasya, "Зачем так делать вот 123456789012345678901234567890asdasdfawefffffqweweqdf"));
            messages.Add(new Message("Пофигу мне"));
            messages.Add(new Message(vasya, "Ну..."));

            listBox2.ItemsSource = msgbtlst;
            UpdateMessages(messages);
        }

       private void UpdateMessages(List<Message> messages)
       {
           msgbtlst.Clear();
           foreach(var now in messages)
           {
               Button bt = new Button();
               bt.Content = now.author+": "+now.text;
               if (now.my)
               {
                   bt.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                   TextBlock tb = new TextBlock();
                   if (tb.Text.Length > 70)
                       tb.Width = 300;
                   tb.Text = Transit.Nick + ": " + now.text;
                   tb.TextWrapping = TextWrapping.Wrap;
                   bt.Content = tb;
                   bt.Background = Transit.MyMessageBrush;
               }
               else
               {
                   TextBlock tb = new TextBlock();
                   tb.Text = now.author.nick + ": " + now.text;
                   if (tb.Text.Length>70)
                       tb.Width = 300;
                   tb.TextWrapping = TextWrapping.Wrap;
                   bt.Content = tb;
                   bt.Background = Transit.HimMessageBrush;
               }
               bt.FontSize = 16;
               bt.BorderBrush = new SolidColorBrush(Color.FromRgb(255, 255, 255));
               bt.Focusable = false;
               bt.IsTabStop = false;
               msgbtlst.Add(bt);
           }
           listBox2.UpdateLayout();

       }

       private void button2_Click(object sender, RoutedEventArgs e)
       {

       }

    }
}
